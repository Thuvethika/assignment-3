#include<stdio.h>
/* program to find factors of a given number */
int main()
{
	int num=0,i=0;
	printf("Enter a positive integer:");
	scanf("%d",&num);
	printf("Factors of %d are :",num);
	
	for(i=1;i<=num;i++) {
		if (num%i == 0)
			printf("%d ",i);
	}
	printf("\n");
	
	return 0;
}

