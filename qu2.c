#include<stdio.h>
/* program to determine the given number is a prime number */
int main()
{
	int number=0,i=0,j=0;
	printf("Enter a number: ");
	scanf("%d", &number);
	
	for(i=2;i<=number/2;i++)
	{
		if (number%i==0)
		{
			j++;
			break;
		}
	}
	if (j==0 && number!=1)
		printf("%d is a prime number\n",number);
	else 
		printf("%d is not a prime number\n",number);
	
return 0;
}

